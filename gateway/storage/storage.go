package storage

import (
	"bytes"
	"errors"
	"sync"

	"github.com/prometheus/client_golang/prometheus"
	"gitlab.com/andycarton/itfb-go-test-task/gateway/behavior"
	"gitlab.com/andycarton/itfb-go-test-task/metrics"
	"gitlab.com/andycarton/itfb-go-test-task/service"
)

// ErrDocNotFound - документ не найден в хранилище
var ErrDocNotFound = errors.New("document not found")

// Simple - модель
type Simple struct {
	sync.RWMutex
	data map[string]node // данные
	// ttl  time.Duration   // время жизни документа - пока не используется
}

type node struct {
	src    *service.Node // исходник
	render []byte        // рендер
	err    error         // последняя ошибка рендера
	// created  time.Time     // дата сохранения - пока не используется
	// accepted time.Time     // дата последнего доступа - пока не используется
}

// NewSimple - какое-то простое хранилище
func NewSimple() *Simple {
	s := Simple{data: make(map[string]node)}
	go s.clean()

	return &s
}

// Set - сохраняет документ
func (s *Simple) Set(doc *service.Document, renderer behavior.Renderer) error {
	obs := prometheus.NewTimer(prometheus.ObserverFunc(func(v float64) {
		metrics.LatencyRepository.WithLabelValues("SimpleStorage", "Set").Observe(v)
	}))
	defer obs.ObserveDuration()

	var w bytes.Buffer
	var n node

	n.err = renderer.Render(doc, &w)
	n.src = doc.Root
	n.render = w.Bytes()

	s.Lock()
	s.data[doc.Name] = n
	s.Unlock()

	return n.err
}

// Get - возвращает документ
func (s *Simple) Get(key string) (*service.Document, error) {
	obs := prometheus.NewTimer(prometheus.ObserverFunc(func(v float64) {
		metrics.LatencyRepository.WithLabelValues("SimpleStorage", "Get").Observe(v)
	}))
	defer obs.ObserveDuration()

	defer s.RUnlock()
	s.RLock()

	n, ok := s.data[key]
	if !ok {
		return nil, ErrDocNotFound
	}

	return &service.Document{Name: key, Root: n.src}, nil
}

// GetRender - возвращает рендер документа
func (s *Simple) GetRender(key string) ([]byte, error) {
	obs := prometheus.NewTimer(prometheus.ObserverFunc(func(v float64) {
		metrics.LatencyRepository.WithLabelValues("SimpleStorage", "GetRender").Observe(v)
	}))
	defer obs.ObserveDuration()

	defer s.RUnlock()
	s.RLock()

	n, ok := s.data[key]
	if !ok {
		return nil, ErrDocNotFound
	}

	if n.err != nil {
		return nil, n.err
	}

	return n.render, nil
}

// clean - удаляет старые документы из хранилища
func (s *Simple) clean() {
	// TODO
	// for {}
}
