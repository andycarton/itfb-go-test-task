package handler

import (
	"github.com/gin-gonic/gin"
)

// VERSION - mw
func VERSION(version string) gin.HandlerFunc {
	return func(c *gin.Context) {
		c.Writer.Header().Set("x-version", version)
		c.Next()
	}
}
