package renderer

import (
	"bytes"
	"testing"

	"gitlab.com/andycarton/itfb-go-test-task/service"
)

var fixRenderNode = &service.Node{
	Type: "html",
	Content: []*service.Node{
		{Type: "head"},
		{
			Type:  "body",
			Attrs: map[string]string{"style": "margin:0;padding:0"},
			Content: []*service.Node{
				{Type: "div", Attrs: map[string]string{"style": "width:25%"}},
				{
					Type:  "div",
					Attrs: map[string]string{"style": "width:50%"},
					Content: []*service.Node{
						{Type: "p", Text: "Текст"},
					},
				},
				{Type: "div", Attrs: map[string]string{"style": "width:25%"}},
			},
		},
	},
}

// TODO больше бенчей
func Benchmark_renderNode(b *testing.B) {
	var w bytes.Buffer
	w.Grow(1024)

	for i := 0; i < b.N; i++ {
		renderNode(fixRenderNode, &w)
		w.Reset()
	}
}
