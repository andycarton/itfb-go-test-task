package renderer

import (
	"bytes"
	"testing"

	"gitlab.com/andycarton/itfb-go-test-task/service"
)

// TODO больше тестов
func Test_wrapNode(t *testing.T) {
	type args struct {
		nodes    []*service.Node
		textNode []byte
	}
	tests := []struct {
		name    string
		args    args
		wantW   string
		wantErr bool
	}{
		{
			name: "0",
			args: args{
				nodes: []*service.Node{
					{Type: "p"},
					{Type: "strong"},
				},
				textNode: []byte("Текст"),
			},
			wantW: "<p><strong>Текст</strong></p>",
		},
		{
			name: "1",
			args: args{
				nodes: []*service.Node{
					{Type: "p"},
					{Type: "strong", Attrs: map[string]string{"style": "border:1px dotted red"}},
				},
				textNode: []byte("Текст"),
			},
			wantW: `<p><strong style="border:1px dotted red">Текст</strong></p>`,
		},
		{
			name: "2",
			args: args{
				textNode: []byte("Текст"),
			},
			wantW: `Текст`,
		},
		{
			name: "3",
			args: args{
				nodes: []*service.Node{
					{Type: "div", Attrs: map[string]string{"style": "border:1px dotted red"}},
					{Type: "p"},
					{Type: "strong"},
					{Type: "span"},
				},
				textNode: []byte("Текст"),
			},
			wantW: `<div style="border:1px dotted red"><p><strong><span>Текст</span></strong></p></div>`,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			w := &bytes.Buffer{}
			if err := wrapNode(tt.args.nodes, tt.args.textNode, w); (err != nil) != tt.wantErr {
				t.Errorf("wrapNode() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if gotW := w.String(); gotW != tt.wantW {
				t.Errorf("wrapNode() = %v, want %v", gotW, tt.wantW)
			}
		})
	}
}

func Test_renderNode(t *testing.T) {
	type args struct {
		node *service.Node
	}
	tests := []struct {
		name    string
		args    args
		wantW   string
		wantErr bool
	}{
		{
			name:  "0",
			wantW: `<html><head><script src="script.js"></script></head><body style="margin:0;padding:0"><div style="width:25%"></div><div style="width:50%"><p>Текст</p></div><div style="width:25%"></div><script src="counters.js"></script></body></html>`,
			args: args{
				node: &service.Node{
					Type: "html",
					Content: []*service.Node{
						{
							Type: "head",
							Content: []*service.Node{
								{Type: "script", Attrs: map[string]string{"src": "script.js"}},
							},
						},
						{
							Type:  "body",
							Attrs: map[string]string{"style": "margin:0;padding:0"},
							Content: []*service.Node{
								{Type: "div", Attrs: map[string]string{"style": "width:25%"}},
								{
									Type:  "div",
									Attrs: map[string]string{"style": "width:50%"},
									Content: []*service.Node{
										{Type: "p", Text: "Текст"},
									},
								},
								{Type: "div", Attrs: map[string]string{"style": "width:25%"}},
								{Type: "script", Attrs: map[string]string{"src": "counters.js"}},
							},
						},
					},
				},
			},
		},
		{
			name:  "1",
			wantW: "Текст",
			args: args{
				node: &service.Node{
					Type: "text",
					Text: "Текст",
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			w := &bytes.Buffer{}
			if err := renderNode(tt.args.node, w); (err != nil) != tt.wantErr {
				t.Errorf("renderNode() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if gotW := w.String(); gotW != tt.wantW {
				t.Errorf("renderNode() = %v, want %v", gotW, tt.wantW)
			}
		})
	}
}
