package renderer

import (
	"errors"
	"html"
	"io"

	"gitlab.com/andycarton/itfb-go-test-task/service"
)

// ErrEmptyNode - получена nil Node
var ErrEmptyNode = errors.New("simple renderer: empty node")

// Преамбула HTML документа
const praeambulus = `<!doctype html>`

// Особенные тэги, br, hr ...
var special = map[string]bool{"br": true, "hr": true}

// SimpleRenderer - какой-то простой рендерер
type SimpleRenderer struct{}

// NewSimple - новый какой-то простой рендерер
func NewSimple() *SimpleRenderer {
	return &SimpleRenderer{}
}

// Render - производит HTML рендер документа
func (r *SimpleRenderer) Render(doc *service.Document, w io.Writer) error {
	_, err := w.Write([]byte(praeambulus))
	if err != nil {
		return err
	}
	return renderNode(doc.Root, w)
}

// renderNode - выводит ноду сразу во writer
// WARNING! не безопасно. тут реккурсия и если указатели зациклены то функция подвиснет
// TODO context с таймаутом
func renderNode(node *service.Node, w io.Writer) error {
	if node == nil {
		return ErrEmptyNode
	}

	switch node.Type {
	case "":
	case "text":
		// выводим обёртку и текст
		// выводим текст
		err := wrapNode(node.Marks, []byte(html.EscapeString(node.Text)), w)
		if err != nil {
			return err
		}
	default:
		// TODO тут можно воспользоваться html.Render(w io.Writer, n *Node) error через адаптер и сравнить бенчмарки
		// выводим открывающий тэг
		err := startTag(node, w)
		if err != nil {
			return err
		}

		// выводим текст
		_, err = w.Write([]byte(html.EscapeString(node.Text)))
		if err != nil {
			return err
		}

		// выводим дочерние ноды
		for _, child := range node.Content {
			if child == nil {
				continue
			}
			renderNode(child, w)
		}

		// выводим закрывающий тэг
		err = endTag(node, w)
		if err != nil {
			return err
		}
	}
	return nil
}

// wrapNode - выводим обёртку
func wrapNode(nodes []*service.Node, textNode []byte, w io.Writer) (err error) {
	for _, node := range nodes {
		err = startTag(node, w)
		if err != nil {
			return err
		}

		defer func(node *service.Node) {
			err = endTag(node, w)
		}(node)
	}

	// выводим текст
	_, err = w.Write(textNode)
	return err
}

// startTag - выводим открывающий тэг
func startTag(node *service.Node, w io.Writer) error {
	_, err := w.Write([]byte{'<'})
	if err != nil {
		return err
	}
	_, err = w.Write([]byte(node.Type))
	if err != nil {
		return err
	}

	// выводим аттрибуты
	if node.Attrs != nil {
		for name, value := range node.Attrs {
			_, err = w.Write([]byte{' '})
			if err != nil {
				return err
			}

			_, err = w.Write([]byte(name))
			if err != nil {
				return err
			}

			if value == "" { // RFC 1866, page 15
				continue
			}

			_, err = w.Write([]byte{'=', '"'})
			if err != nil {
				return err
			}
			_, err := w.Write([]byte(html.EscapeString(value)))
			if err != nil {
				return err
			}
			_, err = w.Write([]byte{'"'})
			if err != nil {
				return err
			}
		}
	}

	// закрываем тэг если "особенный"
	if _, ok := special[node.Type]; ok {
		_, err = w.Write([]byte{'/'})
		if err != nil {
			return err
		}
	}

	_, err = w.Write([]byte{'>'})
	return err
}

// endTag - выводим закрывающий тэг
func endTag(node *service.Node, w io.Writer) error {
	// ничего не делаем если тэг "особенный"
	if _, ok := special[node.Type]; ok {
		return nil
	}

	_, err := w.Write([]byte{'<', '/'})
	if err != nil {
		return err
	}
	_, err = w.Write([]byte(node.Type))
	if err != nil {
		return err
	}
	_, err = w.Write([]byte{'>'})
	return err
}
