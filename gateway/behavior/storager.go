package behavior

import "gitlab.com/andycarton/itfb-go-test-task/service"

// Storager - хранилище
type Storager interface {
	Get(key string) (*service.Document, error)          // забрать документ из хранилища
	GetRender(key string) ([]byte, error)               // забрать рендер из хранилища
	Set(doc *service.Document, renderer Renderer) error // положить документ в хранилище и скомпилировать его
}
