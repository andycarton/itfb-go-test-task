package behavior

import (
	"io"

	"gitlab.com/andycarton/itfb-go-test-task/service"
)

// Renderer .
type Renderer interface {
	Render(*service.Document, io.Writer) error
}
