package gateway

import (
	"context"
	"errors"

	"github.com/prometheus/client_golang/prometheus"
	"gitlab.com/andycarton/itfb-go-test-task/gateway/behavior"
	"gitlab.com/andycarton/itfb-go-test-task/metrics"
	"gitlab.com/andycarton/itfb-go-test-task/service"
)

// Gateway - модель
type Gateway struct {
	storage  behavior.Storager
	renderer behavior.Renderer
}

// New - конструктор
func New(storage behavior.Storager, renderer behavior.Renderer) *Gateway {
	s := Gateway{
		storage:  storage,
		renderer: renderer,
	}

	return &s
}

// Render - Отправляем документ с заполеннным именем в ответ приходит HTML
func (s *Gateway) Render(ctx context.Context, doc *service.Document) (*service.String, error) {
	metrics.Requests.WithLabelValues("Render").Inc()

	obs := prometheus.NewTimer(prometheus.ObserverFunc(func(v float64) {
		metrics.LatencyHandler.WithLabelValues("Render").Observe(v)
	}))
	defer obs.ObserveDuration()

	defer ctx.Done()

	if doc == nil {
		return nil, errors.New("empty document requested")
	}

	blob, err := s.storage.GetRender(doc.Name)

	return &service.String{Value: string(blob)}, err
}

// RenderList - Отправляем список документов с заполеннным именем в ответ приходит список HTML документов
func (s *Gateway) RenderList(ctx context.Context, docList *service.DocumentList) (*service.StringList, error) {
	metrics.Requests.WithLabelValues("RenderList").Inc()

	obs := prometheus.NewTimer(prometheus.ObserverFunc(func(v float64) {
		metrics.LatencyHandler.WithLabelValues("RenderList").Observe(v)
	}))
	defer obs.ObserveDuration()

	defer ctx.Done()

	if docList == nil {
		return nil, errors.New("empty document list requested")
	}

	var list = make([]*service.String, 0, len(docList.Value))
	for i := range docList.Value {
		if docList.Value[i] == nil {
			return nil, errors.New("empty doc input")
		}

		blob, err := s.storage.GetRender(docList.Value[i].Name)
		if err != nil {
			return nil, err
		}

		list = append(list, &service.String{Value: string(blob)})
	}

	return &service.StringList{Value: list}, nil
}

// Save - Отправлем документ и он сохраняется по имени(куда и как на ваше усмотрение)
func (s *Gateway) Save(ctx context.Context, doc *service.Document) (*service.Document, error) {
	metrics.Requests.WithLabelValues("Save").Inc()

	obs := prometheus.NewTimer(prometheus.ObserverFunc(func(v float64) {
		metrics.LatencyHandler.WithLabelValues("Save").Observe(v)
	}))
	defer obs.ObserveDuration()

	defer ctx.Done()

	if doc == nil {
		return nil, errors.New("empty document requested")
	}

	err := s.storage.Set(doc, s.renderer)

	return doc, err
}

// Get - Отправляем документ с заполеннным именем в ответ он возвращается с заполненным деревом
func (s *Gateway) Get(ctx context.Context, doc *service.Document) (*service.Document, error) {
	metrics.Requests.WithLabelValues("Get").Inc()

	obs := prometheus.NewTimer(prometheus.ObserverFunc(func(v float64) {
		metrics.LatencyHandler.WithLabelValues("Get").Observe(v)
	}))
	defer obs.ObserveDuration()

	defer ctx.Done()

	if doc == nil {
		return nil, errors.New("empty document requested")
	}

	return s.storage.Get(doc.Name)
}
