package metrics

import "github.com/prometheus/client_golang/prometheus"

// Merics counters
var (
	Requests = prometheus.NewCounterVec(prometheus.CounterOpts{
		Namespace: "gateway",
		Name:      "requests",
		Help:      "Количество запросов",
	}, []string{"handler_name"})

	LatencyRepository = prometheus.NewSummaryVec(prometheus.SummaryOpts{
		Namespace:  "gateway",
		Name:       "repository_duration",
		Help:       "Отклик от репозиториев данных.",
		Objectives: map[float64]float64{0.5: 0.05, 0.9: 0.01, 0.99: 0.001},
	}, []string{"repository_name", "repository_method"})

	LatencyHandler = prometheus.NewSummaryVec(prometheus.SummaryOpts{
		Namespace:  "gateway",
		Name:       "handler_duration",
		Help:       "Отклик от handler'ов.",
		Objectives: map[float64]float64{0.5: 0.05, 0.9: 0.01, 0.99: 0.001},
	}, []string{"handler_name"})
)

// Register - регистрация метрик
func Register() {
	prometheus.MustRegister(Requests)
	prometheus.MustRegister(LatencyRepository)
	prometheus.MustRegister(LatencyHandler)
}
