package main

import (
	"context"
	"flag"
	"fmt"
	"log"
	"net"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"gitlab.com/andycarton/itfb-go-test-task/gateway"
	"gitlab.com/andycarton/itfb-go-test-task/gateway/handler"
	"gitlab.com/andycarton/itfb-go-test-task/gateway/renderer"
	"gitlab.com/andycarton/itfb-go-test-task/gateway/storage"
	"gitlab.com/andycarton/itfb-go-test-task/metrics"
	"gitlab.com/andycarton/itfb-go-test-task/service"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

var (
	appname = "gateway"      //  название сервиса
	version = "1.0.0"        // версия сервиса  (будет изменен на этапе компиляции)
	build   = "20201120"     // номер билда сервиса (будет изменен на этапе компиляции)
	gwaddr  = "0.0.0.0:7777" // адрес где запуститься gateway
	sysaddr = "0.0.0.0:8080" // адрес где запуститься служебный http

	onTerminate []func() // задачи которые выполнить перед завершением работы
)

func init() {
	printVersionAndExit := false
	printBuildVersionAndExit := false

	flag.BoolVar(&printVersionAndExit, "version", printVersionAndExit, "Вывести номер только версии и выйти")
	flag.BoolVar(&printBuildVersionAndExit, "build", printBuildVersionAndExit, "Вывести номер версии и билда, затем выйти")

	flag.StringVar(&appname, "name", appname, "Имя сервиса")
	flag.StringVar(&gwaddr, "gwaddr", gwaddr, "Адрес сервиса")
	flag.StringVar(&sysaddr, "sysaddr", sysaddr, "Адрес для обслуживания (метрики, пробы)")

	flag.Parse()

	if printVersionAndExit {
		fmt.Print(version)
		os.Exit(0)
	}
	if printBuildVersionAndExit {
		fmt.Print(version + "." + build)
		os.Exit(0)
	}

	log.Println("App version:", version)
	log.Println("App build:", build)
	log.Println("App name:", appname)
	log.Println(appname+" api started at:", "tcp://"+gwaddr)
	log.Println(appname+" sys started at:", "http://"+sysaddr)

	metrics.Register() // регистация счетчиков метрик
}

func main() {
	errchan := make(chan error, 1) // канал сигнализации об аварийном завершении работы

	tcp, err := net.Listen("tcp", gwaddr)
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}

	storage := storage.NewSimple()        // какое-то хранилище
	renderer := renderer.NewSimple()      // какой-то рендерер
	api := gateway.New(storage, renderer) // реа

	gw := grpc.NewServer()
	reflection.Register(gw)
	service.RegisterDocumentServiceServer(gw, api)

	// запуск grpc
	go func() {
		errchan <- gw.Serve(tcp)
	}()
	onTerminate = append(onTerminate, gw.GracefulStop)

	// служебный http сервер. для снятия метрик мониторига prometheus
	// не уверен что /liveness и /readiness пробы нужны докеру, но кубернетесу нужны.
	gin.SetMode(gin.ReleaseMode)
	sys := gin.New()
	sys.Use(gin.Recovery())
	sys.Use(handler.VERSION(version))
	sys.GET("/liveness", handler.Dummy)
	sys.GET("/readiness", handler.Dummy)
	sys.GET("/metrics", func(c *gin.Context) { promhttp.Handler().ServeHTTP(c.Writer, c.Request) })

	internal := http.Server{Addr: sysaddr}
	internal.Handler = sys

	// запуск системного http
	go func() {
		errchan <- internal.ListenAndServe()
	}()
	onTerminate = append(onTerminate, func() { internal.Shutdown(context.TODO()) })

	<-term(errchan)
	<-time.After(20 * time.Millisecond) // ждем последних записей в лог
}

// term .
func term(errchan chan error) chan struct{} {
	term := make(chan struct{}) // канал сигнализации о завершении работы
	sigChan := make(chan os.Signal, 1)

	signal.Notify(sigChan, syscall.SIGINT, syscall.SIGTERM)

	go func() {
		select {
		case <-sigChan:
		case err := <-errchan:
			log.Println(appname+": fatal", err)
		}

		log.Println(appname + ": termitating...")
		for _, job := range onTerminate {
			job()
		}
		term <- struct{}{}
		log.Println(appname + ": termitated")
	}()

	return term
}
