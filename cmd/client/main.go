package main

import (
	"context"
	"log"

	"gitlab.com/andycarton/itfb-go-test-task/service"
	"google.golang.org/grpc"
)

const (
	gateway = "localhost:7070"
)

var sample = service.Document{
	Name: "sample",
	Root: &service.Node{
		Type: "html",
		Content: []*service.Node{
			{
				Type: "head",
				Content: []*service.Node{
					{Type: "script", Attrs: map[string]string{"src": "script.js"}},
				},
			},
			{
				Type:  "body",
				Attrs: map[string]string{"style": "margin:0;padding:0"},
				Content: []*service.Node{
					{Type: "div", Attrs: map[string]string{"style": "width:25%"}},
					{
						Type:  "div",
						Attrs: map[string]string{"style": "width:50%"},
						Content: []*service.Node{
							{Type: "p", Text: "Текст из sample"},
						},
					},
					{Type: "div", Attrs: map[string]string{"style": "width:25%"}},
					{Type: "script", Attrs: map[string]string{"src": "counters.js"}},
				},
			},
		},
	},
}

var sample2 = service.Document{
	Name: "sample2",
	Root: &service.Node{
		Type: "html",
		Content: []*service.Node{
			{
				Type: "head",
				Content: []*service.Node{
					{Type: "script", Attrs: map[string]string{"src": "script.js"}},
				},
			},
			{
				Type:  "body",
				Attrs: map[string]string{"style": "margin:0;padding:0"},
				Content: []*service.Node{
					{Type: "div", Attrs: map[string]string{"style": "width:25%"}},
					{
						Type:  "div",
						Attrs: map[string]string{"style": "width:50%"},
						Content: []*service.Node{
							{Type: "p", Text: "Текст из sample2"},
						},
					},
					{Type: "div", Attrs: map[string]string{"style": "width:25%"}},
					{Type: "script", Attrs: map[string]string{"src": "counters2.js"}},
				},
			},
		},
	},
}

func main() {
	conn, err := grpc.Dial(gateway, grpc.WithInsecure())
	if err != nil {
		log.Fatalf("did not connect: %v", err)
	}
	defer conn.Close()

	cli := service.NewDocumentServiceClient(conn)

	r, err := cli.Save(context.Background(), &sample)
	if err != nil {
		log.Fatal(err)
	}
	log.Printf("Client SET DOCUMENT: %v\n==", r)

	r, err = cli.Get(context.Background(), &sample)
	if err != nil {
		log.Fatal(err)
	}
	log.Printf("Client GET DOCUMENT: %#v\n==", r)

	html, err := cli.Render(context.Background(), &sample)
	if err != nil {
		log.Fatal(err)
	}
	log.Printf("Client GET RENDER DOCUMENT: %s\n==", html)

	r, err = cli.Save(context.Background(), &sample2)
	if err != nil {
		log.Fatal(err)
	}
	log.Printf("Client SET DOCUMENT: %v\n==", r)

	rq := service.DocumentList{
		Value: []*service.Document{
			{Name: "sample"},
			{Name: "sample2"},
		},
	}

	html2, err := cli.RenderList(context.Background(), &rq)
	if err != nil {
		log.Fatal(err)
	}
	log.Printf("Client GET RENDER DOCUMENT: %s\n==", html2)
}
