# ITFB Go Test Task
```
  -build
        Вывести номер версии и билда, затем выйти
  -gwaddr string
        Адрес сервиса (default "0.0.0.0:7777")
  -name string
        Имя сервиса (default "gateway")
  -sysaddr string
        Адрес для обслуживания (метрики, пробы) (default "0.0.0.0:8080")
  -version
        Вывести номер только версии и выйти
```